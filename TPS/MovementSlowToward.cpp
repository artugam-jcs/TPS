#include "MovementSlowToward.h"

#include "MapManager.h"

#include "GameObject.h"
#include "MovableObject.h"

MovementSlowToward::MovementSlowToward(MovableObject* pMovableObject, GameObject* pTarget)
	: Movement(pMovableObject)
	, m_pTarget(pTarget)
	, m_counter(0)
{

}


MovementSlowToward::~MovementSlowToward()
{
	m_pTarget = nullptr;
}

void MovementSlowToward::Run()
{
	// add movement turn
	++m_counter;

	// Check the turn are able to make movement
	if ((m_counter % 2) == 0)
		return;
		

	if (!m_pTarget || !m_pMovableObject)
		return;

	Map* pMap = MapManager::GetInstance()->GetMap();

	if (!pMap)
		return;

	int targetX = m_pTarget->GetX();
	int targetY = m_pTarget->GetY();

	int thisX = m_pMovableObject->GetX();
	int thisY = m_pMovableObject->GetY();

	// Calculate the distance between this object and target object
	int distanceX = targetX - thisX;
	int distanceY = targetY - thisY;

	// Turn into absolute value
	if (distanceX < 0)
		distanceX = -distanceX;
	if (distanceY < 0)
		distanceY = -distanceY;

	// Do the further one
	if (distanceX > distanceY)
	{
		if (targetX > thisX)
		{
			if (!pMap->CheckCollide(thisX + 1, thisY))
				m_pMovableObject->DeltaX(1);
		}
		else
		{
			if (!pMap->CheckCollide(thisX - 1, thisY))
				m_pMovableObject->DeltaX(-1);
		}
	}
	else
	{
		if (targetY > thisY)
		{
			if (!pMap->CheckCollide(thisX, thisY + 1))
				m_pMovableObject->DeltaY(1);
		}
		else
		{
			if (!pMap->CheckCollide(thisX, thisY - 1))
				m_pMovableObject->DeltaY(-1);
		}
	}

}

