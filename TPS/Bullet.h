#ifndef __BULLET_H__
#define __BULLET_H__

#include "MovableObject.h"
#include "Direction.h"


class Shooter;

class Bullet
    : public MovableObject
{
private:
    Direction m_direction;
	Shooter* m_pShooter;

public:
    Bullet(int x = 0, int y = 0, char symbol = '&', ColorType color = sk_defaultColor);
    virtual ~Bullet();

    virtual void Update() override;
    virtual void Draw() override;

    void Execute(int x, int y, Direction dir);
    void Kill();

    // setter
    void SetDirection(const Direction dir) { this->m_direction = dir; }
	void SetShooter(Shooter* shooter) { this->m_pShooter = shooter; }

    // getter
    Direction GetDirection() const { return this->m_direction; }

protected:
    virtual void DoMovement() override;
};

#endif // __BULLET_H__

