#ifndef __GAME_H__
#define __GAME_H__


class InputManager;

class GameObject;

class Obstacle;
class Bullet;

// Map
class Map;
class TestMap;

// Managers
#include "MapManager.h"

#ifndef __ENDGAME_CONDITION__
#define __ENDGAME_CONDITION__

// Simple Enum Handle Lose Condition
enum class EndGameCondition
{
    k_outOfMap,
    k_getKilled,
    k_justExit
};

#endif // __ENDGAME_CONDITION__

class Game
{
public:
    static const uint32 sk_totalEnemies;

private:
	bool m_isEndGame;

    // [Jen-Chieh] Since i am calling thing multiple time and during run time.
    // I think that are better i just hold these two variables instead of calling them
    // from GameManager to save peformance. Indeed, the architect of this game are really bad.
    // Hope this help that i did not over enginereeing thing again.
    // 
    // Since we are not design the framework of console base game. Better way to do this, 
    // will let the "Application" class to handle it! :p
    int m_screenWidth, m_screenHeight;

    // Managers
	InputManager* m_pInputManager;
    MapManager* m_pMapManager;

    // GameObject
    MovableObject* m_pPlayer;
    MovableObject* m_pPlayer2;
    MovableObject* m_pEnemies[200];        // must set to the same as "sk_totalEnemies"

    Map* pMap;
    uint32 m_waveCount;     // count of the enemy wave
    uint32 m_waveCounter;
    uint32 m_totalKillCount;

    // if this is lower then it will spawn the wave faster
    // if higher then spawn enemy slower
    uint32 m_waveDelay;

    // How many enemies we want to spawn in a wave
    uint32 m_enemiesPerWave;

    // Kill count we need to reach untill we spawn the next wave
    uint32 m_reachKillCount;

    // Some Game Setting
    EndGameCondition m_endGameReason;       // could be set at "GameManger", "GameSetting" class, etc.
    int m_enemyOnScreen;
    bool m_isSpawning;

public:
	Game();
	virtual ~Game();

	void Run();				// Run

	// setter
	void SetIsEndGame(const bool isEndGame) { this->m_isEndGame = isEndGame; }

	// getter
	bool GetIsEndGame() const { return this->m_isEndGame; }

private:
	void Initialize();		// Init Game
	void Instruction();		// Print Instruction

	void Update();
	void Draw();

    void Script();
    void SpawnEnemy(int32 deltaEnemyLength);
    void WaveHandler(uint32 waveCount);


    void DrawMap() { if (m_pMapManager) m_pMapManager->Draw(); }
};

#endif // __GAME_H__

