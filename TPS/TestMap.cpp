#include "TestMap.h"


TestMap::TestMap()
{
	// Design Map Here

	// Left
	m_pObstacle[0] = new Obstacle(1, 5);
	m_pObstacle[1] = new Obstacle(1, 6);
	m_pObstacle[2] = new Obstacle(1, 7);
	/*m_pObstacle[3] = new Obstacle(1, 8);
	m_pObstacle[4] = new Obstacle(1, 9);
	m_pObstacle[5] = new Obstacle(1, 10);*/
	m_pObstacle[6] = new Obstacle(1, 11);
	m_pObstacle[7] = new Obstacle(1, 12);
	m_pObstacle[8] = new Obstacle(1, 13);
	m_pObstacle[9] = new Obstacle(1, 14);		// corner

	// Right
	m_pObstacle[10] = new Obstacle(2, 14);
	m_pObstacle[11] = new Obstacle(3, 14);
	m_pObstacle[12] = new Obstacle(4, 14);
	m_pObstacle[13] = new Obstacle(5, 14);
	m_pObstacle[14] = new Obstacle(6, 14);
	m_pObstacle[15] = new Obstacle(7, 14);
	/*m_pObstacle[16] = new Obstacle(8, 14);
	m_pObstacle[17] = new Obstacle(9, 14);
	m_pObstacle[18] = new Obstacle(10, 14);*/
	m_pObstacle[19] = new Obstacle(11, 14);
	m_pObstacle[20] = new Obstacle(12, 14);
	m_pObstacle[21] = new Obstacle(13, 14);
	m_pObstacle[22] = new Obstacle(14, 14);
	m_pObstacle[23] = new Obstacle(15, 14);
	m_pObstacle[24] = new Obstacle(16, 14);		// corner

	// Bottom
	m_pObstacle[25] = new Obstacle(16, 13);
	m_pObstacle[26] = new Obstacle(16, 12);
	m_pObstacle[27] = new Obstacle(16, 11);
	/*m_pObstacle[28] = new Obstacle(16, 10);
	m_pObstacle[29] = new Obstacle(16, 9);
	m_pObstacle[31] = new Obstacle(16, 8);*/
	m_pObstacle[32] = new Obstacle(16, 7);
	m_pObstacle[33] = new Obstacle(16, 6);
	m_pObstacle[34] = new Obstacle(16, 5);		// corner

	// Top
	m_pObstacle[35] = new Obstacle(2, 5);
	m_pObstacle[36] = new Obstacle(3, 5);
	m_pObstacle[37] = new Obstacle(4, 5);
	m_pObstacle[38] = new Obstacle(5, 5);
	m_pObstacle[39] = new Obstacle(6, 5);
	m_pObstacle[40] = new Obstacle(7, 5);
	m_pObstacle[41] = new Obstacle(8, 5);
	/*m_pObstacle[42] = new Obstacle(9, 5);
	m_pObstacle[43] = new Obstacle(10, 5);
	m_pObstacle[44] = new Obstacle(11, 5);*/
	m_pObstacle[45] = new Obstacle(12, 5);
	m_pObstacle[46] = new Obstacle(13, 5);
	m_pObstacle[47] = new Obstacle(14, 5);
	m_pObstacle[48] = new Obstacle(15, 5);
	m_pObstacle[49] = new Obstacle(16, 5);
}

TestMap::~TestMap()
{
	
}


void TestMap::Update()
{
	Map::Update();

	// TODO: Camera Logic
}
