#ifndef __OBJECTSTATUS_H__
#define __OBJECTSTATUS_H__

enum class ObjectStatus
{
	k_alive,
	k_dead,
	k_hidden
};

#endif // __OBJECTSTATUS_H__

