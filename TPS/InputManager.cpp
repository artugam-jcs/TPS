#include "InputManager.h"

#include <conio.h>

InputManager* InputManager::s_pInputManager = nullptr;

InputManager::InputManager()
	: m_key(0)
{

}

InputManager::~InputManager()
{
	s_pInputManager = nullptr;
}

void InputManager::Update()
{
    if (_kbhit())
	    this->m_key = _getch();
}


