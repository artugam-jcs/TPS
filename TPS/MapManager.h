#ifndef __MAPMANAGER_H__
#define __MAPMANAGER_H__

#include "MapType.h"

#include "Map.h"

class MapManager
{
protected:
	static MapManager* s_pMapManager;

	Map* m_pMap;		// Global Map

public:
	MapManager();
	virtual ~MapManager();

	static MapManager* GetInstance()
	{
		if (!s_pMapManager)
			s_pMapManager = new MapManager;
		return s_pMapManager;
	}

	void Update();
	void Draw();

	void SwitchMap(MapType type);

	// getter
	Map* GetMap() const { return this->m_pMap; }

};

#endif // __MAPMANAGER_H__

