#ifndef __CAMERA_H__
#define __CAMERA_H__

#include "InputManager.h"
#include "MovableObject.h"

class Camera
	: public GameObject
{
protected:
	MovableObject* m_pTarget;		// target we want to follow
	InputManager* m_pInputManager;

public:
	Camera(int x, int y, InputManager* pInputManager = nullptr);
	virtual ~Camera();

	virtual void Update() override;
};

#endif // __CAMERA_H__

