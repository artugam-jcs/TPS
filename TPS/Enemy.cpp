#include "Enemy.h"


#include "MapManager.h"

Enemy::Enemy(int x, int y, char symbol, ColorType color, int delay)
    : MovableObject(x, y, symbol, color)
    , m_pTarget(nullptr)
    , m_moveCounter(0)
    , m_delay(delay)
{

}

Enemy::~Enemy()
{

}

void Enemy::Update()
{

    ++m_moveCounter;

    if ((m_moveCounter % m_delay) != 0)
        return;

    // Track Enemies
    if (TrackPlayer())
        return;

    // Do something else not decided yet

}

bool Enemy::TrackPlayer()
{
    if (m_pTarget == nullptr)
        return false;

    if (m_pTarget->GetObjectStatus() == ObjectStatus::k_hidden)
        return false;

    int targetX = m_pTarget->GetX();
    int targetY = m_pTarget->GetY();

    int thisX = this->GetX();
    int thisY = this->GetY();

    int verCom = targetX - this->GetX();
    int horCom = targetY - this->GetY();

    // Change to Absolute Value
    if (verCom < 0) verCom = -verCom;
    if (horCom < 0) horCom = -horCom;

    Map* pMap = MapManager::GetInstance()->GetMap();
    if (pMap == nullptr)
    {
        Debug::Log("Enemy", "(61) Map are null...");
        return false;
    }

    // Do movement
    if (horCom < verCom)
    {
        if (targetX == thisX) {

        }
        else if (targetX < thisX) {
            int tempMoveX = this->m_x - 1;
            if (!pMap->CheckCollide(tempMoveX, this->m_y))
                DeltaX(-1);
        }
        else if (targetX >= thisX) {
            int tempMoveX = this->m_x + 1;
            if (!pMap->CheckCollide(tempMoveX, this->m_y))
                DeltaX(1);
        }

    }
    else {
        // 
        if (targetY == thisY) {

        }
        else if (targetY < thisY) {
            int tempMoveY = this->m_y - 1;
            if (!pMap->CheckCollide(this->m_x, tempMoveY))
                DeltaY(-1);
        }
        else if (targetY >= thisY) {
            int tempMoveY = this->m_y + 1;
            if (!pMap->CheckCollide(this->m_x, tempMoveY))
                DeltaY(1);
        }

    }

    return true;
}

