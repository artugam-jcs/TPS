#ifndef __MOVETYPE_H__
#define __MOVETYPE_H__


//---------------------------------------------------------------------------
// Any MovableObject are allow to use these algorithm in there code
//
// Desc : Create Movement(Base Object) in order to implement movement.
//		  Here list all the movement. 
//---------------------------------------------------------------------------
enum class MoveType
{
	k_none = 0x00,
	k_horizontal = 0x01,
	k_vertical = 0x02,
	k_tightRing = 0x03,
	k_toward = 0x04,
	k_length = 0x05
};

#endif // __MOVETYPE_H__

