#ifndef __MOVEMENTTIGHTRING_H__
#define __MOVEMENTTIGHTRING_H__

#include "Movement.h"

class MovementTightRing
	: public Movement
{
protected:
	int m_range;
	int m_orient;
	int m_counter;
	bool m_isVertical;

public:
	MovementTightRing(MovableObject* pMovableObject, int range, int orient);
	virtual ~MovementTightRing();

	virtual void Run() override;
};

#endif // __MOVEMENTTIGHTRING_H__

