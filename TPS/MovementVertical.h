#ifndef __MOVEMENTVERTICAL_H__
#define __MOVEMENTVERTICAL_H__

#include "Movement.h"

//---------------------------------------------------------------------------------
// MovementVertical
//
// Desc : 
//
// Argument : MovableObject = Object you want to do this movement
//			  range = length object move
//			  orient = which direction u want to move first (+ going down, - going up)
//---------------------------------------------------------------------------------
class MovementVertical
	: public Movement
{
protected:
	int m_counter;
	int m_range;
	int m_orient;

public:
	MovementVertical(MovableObject* pMovableObject, int range, int orient);
	virtual ~MovementVertical();

	virtual void Run() override;
};

#endif // __MOVEMENTVERTICAL_H__

