#include "Bullet.h"


#include "MapManager.h"
#include "Shooter.h"

Bullet::Bullet(int x, int y, char symbol, ColorType color)
    : MovableObject(x, y, symbol, color)
    , m_direction(Direction::K_none)
{
    SetObjectStatus(ObjectStatus::k_hidden);
}

Bullet::~Bullet()
{

}

void Bullet::Update()
{

    // Do movement
    MovableObject::Update();
}

void Bullet::Draw()
{
    MovableObject::Draw();
}

void Bullet::DoMovement()
{
    Map* pMap = MapManager::GetInstance()->GetMap();

    bool kill = false;

    int tempMoveX = this->m_x + m_velocityX;
    if (!pMap->CheckCollide(tempMoveX, this->m_y))
        this->m_x = tempMoveX;
    else
        kill = true;
    
    if (!kill)
    {
        int tempMoveY = this->m_y + m_velocityY;
        if (!pMap->CheckCollide(this->m_x, tempMoveY))
            this->m_y = tempMoveY;
        else 
            kill = true;
    }
    
    if (kill)
    {
        Kill();
        m_pShooter->DeltaNumOfBullet(-1);
    }
}

void Bullet::Execute(int x, int y, Direction dir)
{
    SetDirection(dir);
    SetX(x);
    SetY(y);
    SetObjectStatus(ObjectStatus::k_alive);

    switch (dir)
    {
    case Direction::k_Q:
    {
        this->SetVelocityX(-1);
        this->SetVelocityY(-1);
    } break;
    case Direction::k_W:
    {
        this->SetVelocityX(0);
        this->SetVelocityY(-1);
    } break;
    case Direction::k_E:
    {
        this->SetVelocityX(1);
        this->SetVelocityY(-1);
    } break;
    case Direction::k_D:
    {
        this->SetVelocityX(1);
        this->SetVelocityY(0);
    } break;
    case Direction::k_C:
    {
        this->SetVelocityX(1);
        this->SetVelocityY(1);
    } break;
    case Direction::k_X:
    {
        this->SetVelocityX(0);
        this->SetVelocityY(1);
    } break;
    case Direction::k_Z:
    {
        this->SetVelocityX(-1);
        this->SetVelocityY(1);
    } break;
    case Direction::k_A:
    {
        this->SetVelocityX(-1);
        this->SetVelocityY(0);
    } break;
    }
}

void Bullet::Kill()
{
    SetVelocityX(0);
    SetVelocityY(0);
    SetX(-1);
    SetY(-1);
    SetObjectStatus(ObjectStatus::k_hidden);
}

