#include "Map.h"


Map::Map()
	: m_length(100)
{
	// Init Map to nullptr
	for (int i = 0; i < GetLength(); ++i)
		m_pObstacle[i] = nullptr;
}

Map::~Map()
{
	for (int i = 0; i < GetLength(); ++i)
	{
		if (m_pObstacle[i])
		{
			delete m_pObstacle[i];
			m_pObstacle[i] = nullptr;
		}
	}
}

void Map::Update()
{
	for (int i = 0; i < GetLength(); ++i)
	{
		if (m_pObstacle[i])
			m_pObstacle[i]->Update();
	}
}

void Map::Draw()
{
	for (int i = 0; i < GetLength(); ++i)
	{
		if (m_pObstacle[i])
			m_pObstacle[i]->Draw();
	}
}

bool Map::CheckCollide(int deltaX, int deltaY)
{
	int tempX = deltaX;
	int tempY = deltaY;


	// [Jen-Chieh] Actual map have better performace,
	//			   but i think is worth to have it check
	//			   like this. Easier to organize for programmer
	//			   and to do the level design work.
	//
	// Time Complexity O(n)
	for (int i = 0; i < GetLength(); ++i)
	{
		// Check are readable memory
		if (!m_pObstacle[i])
			continue;

		// Check Position
		if (tempX == this->m_pObstacle[i]->GetX() &&
			tempY == this->m_pObstacle[i]->GetY())
		{
			// Find Collide
			return true;
		}
	}

	return false;
}

