// GameObject.cpp
#include "GameObject.h"

#include "GameGlobals.h"

#include "GameManager.h"

// Set the Default Color of this Game
const ColorType GameObject::sk_defaultColor = ColorType::k_white;

GameObject::GameObject(int x, int y, char symbol, ColorType color)
	: m_x(x)
	, m_y(y)
	, m_uniqueSymbol(symbol)
	, m_currentColor(color)
{

}

GameObject::~GameObject()
{

}

void GameObject::Draw()
{
    // if out of the screen we do not want to render
    if (CheckOutScreen())
        return;

	BeginRender();
	CursorPosition(this->m_x, this->m_y);
	cout << m_uniqueSymbol;		// GameObject's Draw function will just draw the buffer
	EndRender();
}

void GameObject::BeginRender()
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), static_cast<int>(this->m_currentColor));
}

void GameObject::EndRender()
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), static_cast<int>(this->sk_defaultColor));
}

// if out of the screen we do not want to render
bool GameObject::CheckOutScreen()
{
    if (GetX() < 0 || GetX() >= GameManager::s_screenWidth ||
        GetY() < 0 || GetY() >= GameManager::s_screenHeight)
        return true;

    return false;
}

