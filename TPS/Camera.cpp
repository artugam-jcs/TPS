#include "Camera.h"

Camera::Camera(int x, int y, InputManager* pInputManager)
	: GameObject(x, y)
	, m_pTarget(nullptr)
	, m_pInputManager(pInputManager)
{

}

Camera::~Camera()
{
	
}

void Camera::Update()
{
	if (!m_pTarget)
		return;

	this->SetX(m_pTarget->GetX());
	this->SetY(m_pTarget->GetY());



}


