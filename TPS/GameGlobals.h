﻿#ifndef __GAMEGLOBALS_H__
#define __GAMEGLOBALS_H__

#include <windows.h>
#include <iostream>
#include <string>

#include <stdint.h>
#include "ColorType.h"

typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

using std::cout;
using std::endl;
using std::cin;

namespace Debug
{
	//---------------------------------------------------------------------------------
	// Log(string, string)
	// 
	// Desc : Print Specific Debug Code
	//		
	// filename = print location
	// desc : Sentence wants to print
	//---------------------------------------------------------------------------------
	inline void Log(std::string filename, std::string desc)
	{
		std::cout << "[" << filename << "] " << desc << endl;
	}
}

inline void CursorPosition(int row, int column)
{
	COORD coordinate;
	coordinate.X = row;		// 橫 : column
	coordinate.Y = column;			// 直 : row
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coordinate);
}

//---------------------------------------------------------------------------------------------------------------------
// Returns a random number between minValue and maxValue.
// (Integer)
//---------------------------------------------------------------------------------------------------------------------
inline int RandInt(int minValue = 0, int maxValue = 1)
{
	return (rand() % ((maxValue + 1) - minValue)) + minValue;
}

inline ColorType RandColor()
{
    int color = RandInt(1, 6);

    switch (color)
    {
    case 1: return ColorType::k_blue;
    case 2: return ColorType::k_darkGold;
    case 3: return ColorType::k_gold;
    case 4: return ColorType::k_lightBlue;
    case 5: return ColorType::k_lightGreen;
    case 6: return ColorType::k_red;
    }

    // default as white
    return ColorType::k_white;
}

#endif // __GAMEGLOBALS_H__

