#ifndef __MAP_H__
#define __MAP_H__

#include "Obstacle.h"
#include "GameGlobals.h"

class MovableObject;

// Total should be 201(208)
class Map
{
protected:
	uint8 m_length;			// 1 byte
	Obstacle* m_pObstacle[100];		// 20 bytes * 50 = 200

public:
	Map();
	virtual ~Map();

	virtual void Update();
	virtual void Draw();

	bool CheckCollide(int deltaX, int deltaY);

	// Getter
	Obstacle* GetObstacleAt(const int index) const { return this->m_pObstacle[index]; }
	int GetLength() const { return this->m_length; }
};

#endif // __MAP_H__

