#include "MovementHorizontal.h"

#include "MovableObject.h"
#include "MapManager.h"

MovementHorizontal::MovementHorizontal(MovableObject* pMovableObject, int range, int orient)
	: Movement(pMovableObject)
	, m_range(range)
	, m_counter(0)
	, m_orient(orient)
{
	// Decide Direction
	(m_orient > 0) ? m_orient = 1 : m_orient = -1;
}


MovementHorizontal::~MovementHorizontal()
{

}

void MovementHorizontal::Run()
{
	Map* pMap = MapManager::GetInstance()->GetMap();

	// Check are readable memory
	if (!pMap)
		return;

	if (m_counter < m_range)
	{
		if (!pMap->CheckCollide(m_pMovableObject->GetX() + m_orient, m_pMovableObject->GetY()))
			m_pMovableObject->DeltaX(m_orient);
		++m_counter;		// this can be outside of scope or not to
	}
	else
	{
		m_orient = -m_orient;
		m_counter = 0;
	}
}

