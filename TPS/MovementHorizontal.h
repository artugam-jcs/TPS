#ifndef __MOVEMENTHORIZONTAL_H__
#define __MOVEMENTHORIZONTAL_H__

#include "Movement.h"

class MovementHorizontal
	: public Movement
{
protected:
	int m_counter;
	int m_range;
	int m_orient;

public:
	MovementHorizontal(MovableObject* pMovableObject, int range, int orient);
	virtual ~MovementHorizontal();

	virtual void Run() override;
};

#endif // __MOVEMENTHORIZONTAL_H__

