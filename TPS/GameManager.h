#ifndef __GAMEMANAGER_H__
#define __GAMEMANAGER_H__

#include "GameGlobals.h"

class GameManager
{
public:
    static int s_screenWidth, s_screenHeight;

public:
    GameManager();
    ~GameManager();

    static void ResizeWindow();
    static void SetWindow(int, int);
};

#endif // __GAMEMANAGER_H__

