// GameObject.h
#ifndef __GAMEOBJECT_H__
#define __GAMEOBJECT_H__

#include "ColorType.h"

class GameObject
{
public:
	// Default color of this console
	// Could have GameManager Handle it but right now is fine.
	static const ColorType sk_defaultColor;

protected:
	char m_uniqueSymbol;
	int m_x, m_y;

	ColorType m_currentColor;

public:
	GameObject(int x = 0, int y = 0, char symbol = ' ', ColorType color = sk_defaultColor);
	virtual ~GameObject();

	virtual void Update() = 0;
	virtual void Draw();

    bool CheckOutScreen();

	void DeltaX(const int deltaX) { this->m_x += deltaX; }
	void DeltaY(const int deltaY) { this->m_y += deltaY; }

	// setter
	void SetColor(const ColorType color) { this->m_currentColor = color; }
	void SetX(const int x) { this->m_x = x; }
	void SetY(const int y) { this->m_y = y; }

	// getter
	ColorType GetColor() const { return this->m_currentColor; }
	int GetX() const { return this->m_x; }
	int GetY() const { return this->m_y; }
protected:
	void BeginRender();
	void EndRender();

};

#endif // __GAMEOBJECT_H__

