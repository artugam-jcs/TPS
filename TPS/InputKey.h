// InputKey.h
#ifndef __INPUTKEY_H__
#define __INPUTKEY_H__


enum class InputKey
{
    k_q = 113,
    k_w = 0x77,
    k_e = 101,
    k_d = 0x64,
	k_a = 0x61,
    k_c = 99,
    k_x = 120,
    k_z = 122,
	k_s = 0x73,
    k_j = 106,
    k_k = 107,
    k_l = 108,
    k_i = 105,
	k_up = 0x48,
	k_left = 0x4B,
	k_down = 0x50,
	k_right = 0x4D,
	k_space = 0x20,
	k_vk_00 = 0x30,
	k_vk_01 = 0x31,
	k_vk_02 = 0x32,
	k_vk_03 = 0x33,
	k_vk_04 = 0x34,
	k_vk_05 = 0x35,
	k_vk_06 = 0x36,
	k_vk_07 = 0x37,
	k_vk_08 = 0x38,
	k_vk_09 = 0x39,
    k_01 = 49,
    k_02 = 50,
    k_03 = 51,
    k_04 = 52,
    k_05 = 53,
    k_06 = 54,
    k_07 = 55,
    k_08 = 56,
    k_09 = 57,
	k_p = 0x70,
	k_o = 0x6F,
	k_enter = 0x0d,
	k_esc = 0x1B,
	k_temp = 0x40
};

#endif // __INPUTKEY_H__

