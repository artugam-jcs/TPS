#ifndef __ENEMY_H__
#define __ENEMY_H__

#include "MovableObject.h"

class Enemy
    : public MovableObject
{
private:
    MovableObject* m_pTarget;
    int m_moveCounter;
    int m_delay;

public:
    Enemy(int x = 0, int y = 0, char symbol = 'E', ColorType color = sk_defaultColor, int delay = 10);
    virtual ~Enemy();

    virtual void Update() override;

    bool TrackPlayer();

    // setter
    void SetTarget(MovableObject* pTarget) { this->m_pTarget = pTarget; }

    // getter
    MovableObject* GetTarget() const { return this->m_pTarget; }

};

#endif // __ENEMY_H__

