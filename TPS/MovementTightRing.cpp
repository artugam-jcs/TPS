#include "MovementTightRing.h"


#include "MovableObject.h"
#include "MapManager.h"

MovementTightRing::MovementTightRing(MovableObject* pMovableObject, int range, int orient)
	: Movement(pMovableObject)
	, m_range(range)
	, m_orient(orient)
	, m_counter(0)
	, m_isVertical(false)
{
	// Decide Direction
	(m_orient > 0) ? m_orient = 1 : m_orient = -1;
}

MovementTightRing::~MovementTightRing()
{

}

void MovementTightRing::Run()
{
	Map* pMap = MapManager::GetInstance()->GetMap();

	// Check are readable memory
	if (!pMap)
		return;

	// Handle Vertical
	if (m_isVertical)
	{
		if (m_counter < m_range)
		{
			if (!pMap->CheckCollide(m_pMovableObject->GetX(), m_pMovableObject->GetY() + m_orient))
				m_pMovableObject->DeltaY(m_orient);
			++m_counter;
		}
		else
		{
			m_orient = -m_orient;
			m_counter = 0;
			m_isVertical = false;
		}
	}
	// Handle Horizontal
	else
	{
		if (m_counter < m_range)
		{
			if (!pMap->CheckCollide(m_pMovableObject->GetX() + m_orient, m_pMovableObject->GetY()))
				m_pMovableObject->DeltaX(m_orient);
			++m_counter;
		}
		else
		{
			m_counter = 0;
			m_isVertical = true;
		}
	}
}

