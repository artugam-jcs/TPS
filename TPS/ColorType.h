#ifndef __COLORTYPE_H__
#define __COLORTYPE_H__


enum class ColorType
{
	k_white = 0x0F,
	k_gold = 0x0E,
	k_blue = 0x09,
	k_lightGreen = 0x0A,
	k_lightBlue = 0x0B,
	k_red = 0x0C,
	k_darkGold = 0x06
};

#endif // __COLORTYPE_H__

