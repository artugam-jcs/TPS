#ifndef __MOVEMENTFACTORY_H__
#define __MOVEMENTFACTORY_H__

// Enums
#include "MoveType.h"

// Actual Object
#include "MovementVertical.h"
#include "MovementHorizontal.h"
#include "MovementTightRing.h"
#include "MovementSlowToward.h"

class MovementFactory
{
public:

	static Movement* CreateMovement(MoveType moveType, MovableObject* pMovableObject, int range = 3, int orient = 1, GameObject* pTarget = nullptr)
	{
		Movement* pMovement = nullptr;

		switch (moveType)
		{
		case MoveType::k_vertical:
		{
			pMovement = new MovementVertical(pMovableObject, range, orient);
		} break;
		case MoveType::k_horizontal:
		{
			pMovement = new MovementHorizontal(pMovableObject, range, orient);
		} break;
		case MoveType::k_tightRing:
		{
			pMovement = new MovementTightRing(pMovableObject, range, orient);
		} break;
		case MoveType::k_toward:
		{
			pMovement = new MovementSlowToward(pMovableObject, pTarget);
		} break;
		}

		return pMovement;
	}

};

#endif // __MOVEMENTFACTORY_H__

