#ifndef __INPUTMANAGER_H__
#define __INPUTMANAGER_H__

#include "InputKey.h"

class InputManager
{
private:
	static InputManager* s_pInputManager;

	int m_key;

	InputManager();
public:
	virtual ~InputManager();

	static InputManager* GetInstance()
	{
		if (!s_pInputManager)
			s_pInputManager = new InputManager;
		return s_pInputManager;
	}

	void Update();

	// setter
	void SetKey(const int key) { this->m_key = key; }

	// getter
	InputKey GetKey() const { return static_cast<InputKey>(this->m_key); }

};

#endif // __INPUTMANAGER_H__

