// MovableObject.h
#ifndef __MOVABLEOBJECT_H__
#define __MOVABLEOBJECT_H__

#include "GameObject.h"
#include "GameGlobals.h"
#include "ObjectStatus.h"

class Map;

class MovableObject
	: public GameObject
{
protected:
	ObjectStatus m_objectStatus;

	int m_lastPosX, m_lastPosY;
	char m_currentSymbol;

    int m_velocityX, m_velocityY;

public:
	MovableObject(int x, int y, char symbol = ' ', ColorType color = sk_defaultColor);
	virtual ~MovableObject();

	virtual void Update() override;
	virtual void Draw() override;

	// setter
	void SetObjectStatus(const ObjectStatus status) { this->m_objectStatus = status; }
    void SetVelocityX(const int vel) { this->m_velocityX = vel; }
    void SetVelocityY(const int vel) { this->m_velocityY = vel; }

	// getter
	ObjectStatus GetObjectStatus() const { return this->m_objectStatus; }
    int GetVelocityX() const { return this->m_velocityX; }
    int GetVelocityY() const { return this->m_velocityY; }

protected:
    virtual void DoMovement();

	// setter
	void SetCurrentSymbol(char symbol) { this->m_currentSymbol = symbol; }

};

#endif // __MOVABLEOBJECT_H__

