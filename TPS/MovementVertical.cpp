#include "MovementVertical.h"

#include "MovableObject.h"
#include "MapManager.h"

MovementVertical::MovementVertical(MovableObject* pMovableObject, int range, int orient)
	: Movement(pMovableObject)
	, m_range(range)
	, m_counter(0)
	, m_orient(orient)
{
	// Decide Direction
	(m_orient > 0) ? m_orient = 1 : m_orient = -1;
}

MovementVertical::~MovementVertical()
{

}

void MovementVertical::Run()
{
	Map* pMap = MapManager::GetInstance()->GetMap();

	// Check are readable memory
	if (!pMap)
		return;

	if (m_counter < m_range)
	{
		if (!pMap->CheckCollide(m_pMovableObject->GetX(), m_pMovableObject->GetY() + m_orient))
			m_pMovableObject->DeltaY(m_orient);
		++m_counter;
	}
	else
	{
		m_orient = -m_orient;
		m_counter = 0;
	}
	
}

