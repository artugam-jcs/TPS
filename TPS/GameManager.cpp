#include "GameManager.h"


int GameManager::s_screenWidth = 75;
int GameManager::s_screenHeight = 25;

GameManager::GameManager()
{
}


GameManager::~GameManager()
{
}

void GameManager::ResizeWindow()
{
    SetWindow(s_screenWidth, s_screenHeight);
}

void GameManager::SetWindow(int Width, int Height)
{
    _COORD coord;
    coord.X = Width;
    coord.Y = Height;

    _SMALL_RECT Rect;
    Rect.Top = 0;
    Rect.Left = 0;
    Rect.Bottom = Height - 1;
    Rect.Right = Width - 1;

    HANDLE Handle = GetStdHandle(STD_OUTPUT_HANDLE);      // Get Handle 
    SetConsoleScreenBufferSize(Handle, coord);            // Set Buffer Size 
    SetConsoleWindowInfo(Handle, TRUE, &Rect);            // Set Window Size 
}

