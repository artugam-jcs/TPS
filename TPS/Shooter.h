#ifndef __SHOOTER_H__
#define __SHOOTER_H__

#include "GameGlobals.h"
#include "MovableObject.h"

#include "Bullet.h"

class Shooter
    : public MovableObject
{
private:
	Bullet m_bullets[20];
    const uint32 k_sizeOfBullet;
    uint32 m_numOfBullets;
    uint32 m_healthPoint;
    uint32 m_killCount;

public:
    Shooter(int x = 0, int y = 0, char symbol = 'S', ColorType color = sk_defaultColor);
    virtual ~Shooter();


    virtual void Update() override;
    virtual void Draw() override;

    void Execute(Direction dir);


    // setter
	void DeltaNumOfBullet(const int delta) { this->m_numOfBullets += delta; }
    void DeltaKillCount(const int delta) { this->m_killCount += delta; }
    void DeltaHealthPoint(const int delta) { this->m_healthPoint += delta; }
    void SetKillCount(const int count) { this->m_killCount = count; }

    // getter
    uint32 GetHealthPoint() const { return this->m_healthPoint; }
    uint32 GetKillCount() const { return this->m_killCount; }
    Bullet GetBulletAt(const uint32 index) { return m_bullets[index]; }
    const uint32 GetBulletSize() const { return this->k_sizeOfBullet; }

};

#endif // __SHOOTER_H__

