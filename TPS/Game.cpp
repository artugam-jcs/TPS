#include "Game.h"

#include <conio.h>
#include <time.h>

// Useful Headers
#include "GameGlobals.h"
#include "InputKey.h"

// Managers
#include "InputManager.h"

// GameObjects Headers
#include "Obstacle.h"
#include "Shooter.h"
#include "Bullet.h"
#include "Enemy.h"

// Enums
#include "MoveType.h"
#include "ObjectStatus.h"

// Maps
#include "TestMap.h"

// Managers
#include "GameManager.h"


// The maximum if the enemies count render can be set here.
const uint32 Game::sk_totalEnemies = 200;

Game::Game()
	: m_isEndGame(false)
	, m_pInputManager(nullptr)
	, m_pPlayer(nullptr)
    , m_endGameReason(EndGameCondition::k_justExit)
    , m_enemyOnScreen(5)
    , m_screenWidth(GameManager::s_screenWidth)
    , m_screenHeight(GameManager::s_screenHeight)
    , m_waveCount(0)
    , m_waveCounter(0)
    , m_waveDelay(0)
    , m_enemiesPerWave(0)
    , m_totalKillCount(0)
    , m_reachKillCount(0)
{

}

Game::~Game()
{
	if (m_pPlayer)
	{
		delete m_pPlayer;
		m_pPlayer = nullptr;
	}

    if (m_pPlayer2)
    {
        delete m_pPlayer2;
        m_pPlayer2 = nullptr;
    }

	if (m_pInputManager)
	{
		delete m_pInputManager;
		m_pInputManager = nullptr;
	}

	if (m_pMapManager)
	{
		delete m_pMapManager;
		m_pMapManager = nullptr;
	}

    for (int i = 0; i < sk_totalEnemies; ++i)
    {
        if (m_pEnemies[i])
        {
            delete m_pEnemies[i];
            m_pEnemies[i] = nullptr;
        }
    }

}

void Game::Initialize()
{
	SetConsoleTitle("JCS_BackHeist");

	// Generate Random Number
	srand(static_cast<unsigned int>(time(0)));
	
	m_pInputManager = InputManager::GetInstance();
	m_pMapManager = MapManager::GetInstance();

    // Load the map
	m_pMapManager->SwitchMap(MapType::k_testMap);

    // Get the Map
    pMap = m_pMapManager->GetMap();

	// Create Player
	m_pPlayer = new Shooter(3, 7, 'J', ColorType::k_lightBlue);
    m_pPlayer2 = new Shooter(4, 8, 'M', ColorType::k_lightGreen);

    // Create 10 Enemies
    for (int i = 0; i < sk_totalEnemies; ++i)
    {
        // create enemies
        m_pEnemies[i] = new Enemy(RandInt(1, m_screenWidth), RandInt(1, m_screenHeight), 'E', ColorType::k_red, RandInt(20, 50));
        // set the target
        dynamic_cast<Enemy*>(m_pEnemies[i])->SetTarget(dynamic_cast<MovableObject*>(m_pPlayer));

        // Set To Hidden
        if (i > m_enemyOnScreen)
            m_pEnemies[i]->SetObjectStatus(ObjectStatus::k_hidden);
    }

    // Set wave count
    m_waveCount = 1;

    // Set wave delay
    WaveHandler(m_waveCount);

    // Set enemy per wave
    m_enemiesPerWave = 10;

    m_reachKillCount = 3;

    // Resize the window
    GameManager::ResizeWindow();
}

// Main Program Loop
void Game::Run()
{
	// Initialize the Game for use
	Initialize();
	
	// I would like to print the instruction after game are initialize correctly
	Instruction();

    cout << "\nPress ESC to end...\n";
    system("PAUSE");
    system("CLS");

    DrawMap();
    
    InputKey key;

	// [Jen-Chieh] Should have the application class to handle program loop
	//				but the same i would like to keep it simple.
	while (!GetIsEndGame())
	{
		// get the key from player
		m_pInputManager->Update();

		// Update Movement
		Update();

		// Render
		Draw();

        // Delay for a while
        Sleep(25);

        // Check End Game
        key = m_pInputManager->GetKey();
        if (key == InputKey::k_esc)
            SetIsEndGame(true);

        // Ready for next use
        m_pInputManager->SetKey(-1);
	}

    system("CLS");

    // Final Screen design here!~
    {
        // Decide which kind of reason that player end the game.
        switch (m_endGameReason)
        {
        case EndGameCondition::k_getKilled:
        {
            // Get Shooter information
            Shooter* pPlayer1 = nullptr;
            Shooter* pPlayer2 = nullptr;
            pPlayer1 = dynamic_cast<Shooter*>(m_pPlayer);
            pPlayer2 = dynamic_cast<Shooter*>(m_pPlayer2);

            if (pPlayer1 == nullptr)
            {
                Debug::Log("Game.cpp", "\"pPlayer1\" are nullptr but i am trying to access it.");
                return;
            }

            if (pPlayer2 == nullptr)
            {
                Debug::Log("Game.cpp", "\"pPlayer2\" are nullptr but i am trying to access it.");
                return;
            }


            // Get Data from each player and read to display for the result
            int killCount1 = pPlayer1->GetKillCount();
            int killCount2 = pPlayer2->GetKillCount();

            // Display Result
            cout << "Player 1 Kill Count: " << killCount1 << endl;
            cout << "Player 2 Kill Count: " << killCount2 << endl;
            cout << "Wave Count: " << m_waveCount << endl;

            std::string word;
            // Compare the score
            {
                if (killCount1 == killCount2)
                {
                    cout << "Tie!" << endl;
                    break;
                }

                (killCount1 > killCount2) ? word = "\"Player 1\" Beat You!! LOL! :P" : word = "\"Player 2\" Beat You!! LOL! :P";
            }

            // print out the result
            cout << word << endl;

        } break;
        case EndGameCondition::k_outOfMap: 
        {
            cout << "Communicate to eahc other are way important than just kill the enemies..." << endl;
            cout << "Try better next time." << endl;
        } break;
        }

        // Default display screen!
        cout << "\nThanks for playing!~" << endl;
    }

	_getch();
}

//---------------------------------------------------------------------------------
// Should have Scene and SceneManager handle it
// but i would like to keep it simple for this project.
// meaning no switching scene in this game.
//---------------------------------------------------------------------------------
void Game::Instruction()
{
	// Instruction for Player to see
	cout << "RULES of Game :" << endl;
	cout << "\t1. Use w, a, s, d keys to move. (T)" << endl;
	cout << "\t2. The player wins by bringing all bags to the getaway car." << endl;
	cout << "\t3. You loses upon contacting a guard. (G)" << endl;
}

// handle logic
void Game::Update()
{
    Script();

	if (m_pMapManager)
		m_pMapManager->Update();

	if (m_pPlayer)
		m_pPlayer->Update();

    if (m_pPlayer2)
        m_pPlayer2->Update();

    for (int i = 0; i < sk_totalEnemies; ++i)
    {
        if (m_pEnemies[i])
        {
            if (m_pEnemies[i]->GetObjectStatus() != ObjectStatus::k_hidden)
                m_pEnemies[i]->Update();
        }
    }
    
    if (m_totalKillCount < m_reachKillCount)
        return;

    // Do wave counter
    ++m_waveCounter;

    if (m_waveCounter > m_waveDelay)
    {
        SpawnEnemy(m_enemiesPerWave);     // spawn the enemy
        ++m_waveCount;
        WaveHandler(m_waveCount);       // change wave delay
        m_waveCounter = 0;
    }

}

// handle buffer
// Better way to do this should be implement like "Unity" implemented
// Create a Scene Object to take care of all the Renderable Object.
void Game::Draw()
{

	if (m_pPlayer)
		m_pPlayer->Draw();

    if (m_pPlayer2)
        m_pPlayer2->Draw();

    for (int i = 0; i < sk_totalEnemies; ++i)
    {
        if (m_pEnemies[i])
        {
            if (m_pEnemies[i]->GetObjectStatus() != ObjectStatus::k_hidden)
                m_pEnemies[i]->Draw();
        }
    }

}

// All the movement and gameplay contain here
// There are better way to do but ill just have it like this right now...
void Game::Script()
{
    InputKey key = m_pInputManager->GetKey();

    //cout << " Key: "<< (int)key;

    if (Shooter* pPlayer = dynamic_cast<Shooter*>(m_pPlayer))
    {
        if (key == InputKey::k_l)
            pPlayer->SetVelocityX(1);
        else if (key == InputKey::k_j)
            pPlayer->SetVelocityX(-1);
        else 
            pPlayer->SetVelocityX(0);

        if (key == InputKey::k_i)
            pPlayer->SetVelocityY(-1);
        else if (key == InputKey::k_k)
            pPlayer->SetVelocityY(1);
        else
            pPlayer->SetVelocityY(0);

        if (key == InputKey::k_q)
            pPlayer->Execute(Direction::k_Q);
        if (key == InputKey::k_w)
            pPlayer->Execute(Direction::k_W);
        if (key == InputKey::k_e)
            pPlayer->Execute(Direction::k_E);
        if (key == InputKey::k_d)
            pPlayer->Execute(Direction::k_D);
        if (key == InputKey::k_c)
            pPlayer->Execute(Direction::k_C);
        if (key == InputKey::k_x)
            pPlayer->Execute(Direction::k_X);
        if (key == InputKey::k_z)
            pPlayer->Execute(Direction::k_Z);
        if (key == InputKey::k_a)
            pPlayer->Execute(Direction::k_A);
        
        if (key == InputKey::k_p)
            SpawnEnemy(20);

        // Check the collision
        for (int i = 0; i < sk_totalEnemies; ++i)
        {
            if (m_pEnemies[i]->GetObjectStatus() == ObjectStatus::k_hidden)
                continue;

            if (m_pEnemies[i]->GetX() == pPlayer->GetX() &&
                m_pEnemies[i]->GetY() == pPlayer->GetY())
            {
                m_endGameReason = EndGameCondition::k_getKilled;
                pPlayer->DeltaHealthPoint(-1);
            }
        }

        // Bullet and Player Collision
        for (int i = 0; i < sk_totalEnemies; ++i)
        {
            MovableObject* enemy = m_pEnemies[i];
            if (enemy->GetObjectStatus() == ObjectStatus::k_hidden)
                continue;
            
            for (uint32 j = 0; j < pPlayer->GetBulletSize(); ++j)
            {
                Bullet bullet = pPlayer->GetBulletAt(j);
                if (bullet.GetObjectStatus() == ObjectStatus::k_hidden)
                    continue;

                if (enemy->GetX() == bullet.GetX() &&
                    enemy->GetY() == bullet.GetY())
                {
                    enemy->SetObjectStatus(ObjectStatus::k_hidden);
                    bullet.SetObjectStatus(ObjectStatus::k_hidden);
                    pPlayer->DeltaKillCount(1);
                    ++m_totalKillCount;
                }
            }
        }

        // If one of the player is dead end the game,
        // cuz i want player to communicate and able to help each other.
        if (pPlayer->CheckOutScreen())
        {
            m_endGameReason = EndGameCondition::k_outOfMap;
            SetIsEndGame(true);
        }

        if (pPlayer->GetHealthPoint() <= 0)
            SetIsEndGame(true);
    }

    if (Shooter* pPlayer2 = dynamic_cast<Shooter*>(m_pPlayer2))
    {
        // Player 2
        if (key == InputKey::k_right)
            pPlayer2->SetVelocityX(1);
        else if (key == InputKey::k_left)
            pPlayer2->SetVelocityX(-1);
        else
            pPlayer2->SetVelocityX(0);

        if (key == InputKey::k_up)
            pPlayer2->SetVelocityY(-1);
        else if (key == InputKey::k_down)
            pPlayer2->SetVelocityY(1);
        else
            pPlayer2->SetVelocityY(0);


        if (key == InputKey::k_07)
            pPlayer2->Execute(Direction::k_Q);
        if (key == InputKey::k_08)
            pPlayer2->Execute(Direction::k_W);
        if (key == InputKey::k_09)
            pPlayer2->Execute(Direction::k_E);
        if (key == InputKey::k_06)
            pPlayer2->Execute(Direction::k_D);
        if (key == InputKey::k_03)
            pPlayer2->Execute(Direction::k_C);
        if (key == InputKey::k_02)
            pPlayer2->Execute(Direction::k_X);
        if (key == InputKey::k_01)
            pPlayer2->Execute(Direction::k_Z);
        if (key == InputKey::k_04)
            pPlayer2->Execute(Direction::k_A);

        // Check the collision
        for (int i = 0; i < sk_totalEnemies; ++i)
        {
            if (m_pEnemies[i]->GetObjectStatus() == ObjectStatus::k_hidden)
                continue;

            if (m_pEnemies[i]->GetX() == pPlayer2->GetX() &&
                m_pEnemies[i]->GetY() == pPlayer2->GetY())
            {
                m_endGameReason = EndGameCondition::k_getKilled;
                pPlayer2->DeltaHealthPoint(-1);
            }
        }

        // Bullet and Player Collision
        for (int i = 0; i < sk_totalEnemies; ++i)
        {
            MovableObject* enemy = m_pEnemies[i];
            if (enemy->GetObjectStatus() == ObjectStatus::k_hidden)
                continue;

            for (uint32 j = 0; j < pPlayer2->GetBulletSize(); ++j)
            {
                Bullet bullet = pPlayer2->GetBulletAt(j);
                if (bullet.GetObjectStatus() == ObjectStatus::k_hidden)
                    continue;

                if (enemy->GetX() == bullet.GetX() &&
                    enemy->GetY() == bullet.GetY())
                {
                    enemy->SetObjectStatus(ObjectStatus::k_hidden);
                    bullet.SetObjectStatus(ObjectStatus::k_hidden);
                    pPlayer2->DeltaKillCount(1);
                    ++m_totalKillCount;
                }
            }
        }

        // If one of the player is dead end the game,
        // cuz i want player to communicate and able to help each other.
        if (pPlayer2->CheckOutScreen())
        {
            m_endGameReason = EndGameCondition::k_outOfMap;
            SetIsEndGame(true);
        }

        if (pPlayer2->GetHealthPoint() <= 0)
            SetIsEndGame(true);
    }

}

// Adding enemies into map since the wave change
void Game::SpawnEnemy(int32 deltaEnemyLength)
{
    int32 counter = 0;
    uint32 targetNum = 0;

    // Search enemies pool
    for (int i = 0; i < sk_totalEnemies; ++i)
    {
        // Stop adding enemies into map
        if (counter == deltaEnemyLength)
            break;

        // seeking hidden enemy
        if (m_pEnemies[i]->GetObjectStatus() == ObjectStatus::k_hidden)
        {
            while (true)
            {
                // Generate Random Number
                int randX = RandInt(1, m_screenWidth);
                int randY = RandInt(1, m_screenHeight);

                // Check collide, if is not collide then we are good to go
                // we will like to check collide until we get the spaces.
                if (!pMap->CheckCollide(randX, randY))
                {
                    // Set Position
                    m_pEnemies[i]->SetX(randX);
                    m_pEnemies[i]->SetY(randY);
                    break;
                }
            }

            if (Enemy* pEnemy = dynamic_cast<Enemy*>(m_pEnemies[i]))
            {
                targetNum = RandInt(0, 1); // 0 or 1
                (targetNum == 0) ? pEnemy->SetTarget(m_pPlayer) : pEnemy->SetTarget(m_pPlayer2);
            }
            
            m_pEnemies[i]->SetObjectStatus(ObjectStatus::k_alive);

            ++counter;
            ++m_enemyOnScreen;
        }
    }

}

// Design the wave here...
void Game::WaveHandler(uint32 waveCount)
{
    switch (waveCount)
    {
    case 1: { m_waveDelay = 150; m_enemiesPerWave = 5; m_reachKillCount = 5; } break;       // ... (Really BAD)
    case 2: { m_waveDelay = 145; m_enemiesPerWave = 10; m_reachKillCount = 10; } break;       // Fail
    case 3: { m_waveDelay = 120; m_enemiesPerWave = 15; m_reachKillCount = 15; } break;       // normal
    case 4: { m_waveDelay = 105; m_enemiesPerWave = 20; m_reachKillCount = 20; } break;       // Okay
    case 5: { m_waveDelay = 95; m_enemiesPerWave = 25; m_reachKillCount = 25; } break;        // Great
    case 6: { m_waveDelay = 75; m_enemiesPerWave = 30; m_reachKillCount = 30; } break;        // Excellent
    case 7: { m_waveDelay = 60; m_enemiesPerWave = 35; m_reachKillCount = 35; } break;        // Rampage
    case 8: { m_waveDelay = 45; m_enemiesPerWave = 40; m_reachKillCount = 45; } break;        // Godlike
    case 9: { m_waveDelay = 30; m_enemiesPerWave = 45; m_reachKillCount = 45; } break;        // Lengendary
    default: { m_waveDelay = 15; m_enemiesPerWave = 60; m_reachKillCount = 60; } break;       // Game Master
    }
}

