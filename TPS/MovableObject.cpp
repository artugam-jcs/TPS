#include "MovableObject.h"

#include "MapManager.h"

MovableObject::MovableObject(int x, int y, char symbol, ColorType color)
	: GameObject(x, y, symbol, color)
	, m_lastPosX(-1)
	, m_lastPosY(-1)
	, m_currentSymbol(symbol)
    , m_velocityX(0)
    , m_velocityY(0)
{

}


MovableObject::~MovableObject()
{

}

void MovableObject::Update()
{

    if (m_objectStatus == ObjectStatus::k_hidden)
        SetCurrentSymbol(' ');
    else
		SetCurrentSymbol(m_uniqueSymbol);

    if (m_objectStatus == ObjectStatus::k_hidden)
        return;

    DoMovement();
}

void MovableObject::Draw()
{

    // Don't render if we does not change anything
    // Save performance
    if (m_lastPosX == m_x && m_lastPosY == m_y)
        return;

    // Start render
    {
        BeginRender();

        // Clear last frame's position
        CursorPosition(this->m_lastPosX, this->m_lastPosY);
        cout << ' ';

        // Out of screen
        if (CheckOutScreen() || GetObjectStatus() == ObjectStatus::k_hidden)
        {
            SetObjectStatus(ObjectStatus::k_hidden);
            SetVelocityX(0);
            SetVelocityY(0);
            SetX(-1);
            SetY(-1);

            m_lastPosX = -1;
            m_lastPosY = -1;

            return;
        }

        // Draw it on current position
        CursorPosition(this->m_x, this->m_y);
        cout << this->m_currentSymbol;

        // Assign to current position
        this->m_lastPosX = this->m_x;
        this->m_lastPosY = this->m_y;

        EndRender();
    }
}


void MovableObject::DoMovement()
{
    // Doing Movement
    {
        if (GetVelocityX() == 0 && GetVelocityY() == 0)
            return;

        Map* pMap = MapManager::GetInstance()->GetMap();
        if (pMap == nullptr)
        {
            Debug::Log("Shooter", "(46) Map are null...");
            return;
        }

        int tempMoveX = this->m_x + m_velocityX;
        if (!pMap->CheckCollide(tempMoveX, this->m_y))
            this->m_x += m_velocityX;

        int tempMoveY = this->m_y + m_velocityY;
        if (!pMap->CheckCollide(this->m_x, tempMoveY))
            this->m_y += m_velocityY;
    }
}

