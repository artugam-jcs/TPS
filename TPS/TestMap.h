#ifndef __TESTMAP_H__
#define __TESTMAP_H__

#include "Map.h"

class TestMap 
	: public Map
{
public:
	TestMap();
	virtual ~TestMap();

	virtual void Update() override;
};

#endif __TESTMAP_H__

