#ifndef __OBSTACLE_H__
#define __OBSTACLE_H__

#include "GameObject.h"

class Obstacle
	: public GameObject
{
public:
	Obstacle(int x, int y, char symbol = (char)239, ColorType color = ColorType::k_blue);
	virtual ~Obstacle();

	virtual void Update() override;
};

#endif // __OBSTACLE_H__

