#ifndef __MOVEMENT_H__
#define __MOVEMENT_H__


class MovableObject;

class Movement
{
protected:
	MovableObject* m_pMovableObject;

public:
	Movement(MovableObject* pMovableObject)
		: m_pMovableObject(pMovableObject)
	{  }
	virtual ~Movement() { m_pMovableObject = nullptr; }
	virtual void Run() = 0;		// main movement algorithm put here
};

#endif // __MOVEMENT_H__

