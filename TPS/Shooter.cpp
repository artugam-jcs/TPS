#include "Shooter.h"


Shooter::Shooter(int x, int y, char symbol, ColorType color)
    : MovableObject(x, y, symbol, color)
    , m_bullets()
    , k_sizeOfBullet(20)
	, m_healthPoint(100)
    , m_killCount(0)
    , m_numOfBullets(20)
{
    for (int i = 0; i < 20; ++i)
    {
        m_bullets[i].SetShooter(this);
    }
}

Shooter::~Shooter()
{

}

void Shooter::Update()
{
    MovableObject::Update();

    for (uint32 i = 0; i < k_sizeOfBullet; ++i)
        m_bullets[i].Update();

    /*for (int i = 0; i < 20; ++i)
        m_bullets[i].Update();*/
}

void Shooter::Draw()
{
    MovableObject::Draw();

    for (uint32 i = 0; i < k_sizeOfBullet; ++i)
        m_bullets[i].Draw();

    /*for (int i = 0; i < 20; ++i)
        m_bullets[i].Draw();*/
}

void Shooter::Execute(Direction dir)
{
    for (uint32 i = 0; i < k_sizeOfBullet; ++i)
    {
        if (m_bullets[i].GetObjectStatus() == ObjectStatus::k_hidden)
        {
            m_bullets[i].Execute(this->m_x, this->m_y, dir);
			--m_numOfBullets;
            break;
        }
    }
}

