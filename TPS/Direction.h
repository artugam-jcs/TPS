#ifndef __DIRECTION_H__
#define __DIRECTION_H__
/**
Q | W | E
---+---+---
A | S | D
---+---+---
Z | X | C
*/
enum class Direction
{
    K_none,
    k_Q,
    k_W,
    k_E,
    k_D,
    k_C,
    k_X,
    k_Z,
    k_A,
    k_S
};

#endif // __DIRECTION_H__

