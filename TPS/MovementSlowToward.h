#ifndef __MOVEMENTSLOWTOWARD_H__
#define __MOVEMENTSLOWTOWARD_H__


#include "Movement.h"

class GameObject;

class MovementSlowToward
	: public Movement
{
protected:
	GameObject* m_pTarget;
	int m_counter;

public:
	MovementSlowToward(MovableObject* pMovableObject, GameObject* pTarget);
	virtual ~MovementSlowToward();

	virtual void Run() override;
};

#endif // __MOVEMENTSLOWTOWARD_H__

