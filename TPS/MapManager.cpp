#include "MapManager.h"

#include "TestMap.h"

MapManager* MapManager::s_pMapManager = nullptr;

MapManager::MapManager()
	: m_pMap(nullptr)
{
	
}

MapManager::~MapManager()
{
	s_pMapManager = nullptr;
	
	if (m_pMap)
	{
		delete m_pMap;
		m_pMap = nullptr;
	}
}

void MapManager::Update()
{
	GetMap()->Update();
}

void MapManager::Draw()
{
	GetMap()->Draw();
}

void MapManager::SwitchMap(MapType type)
{
	if (m_pMap)
	{
		delete m_pMap;
		m_pMap = nullptr;
	}

	switch (type)
	{
	case MapType::k_testMap:
	{
		m_pMap = new TestMap;
	} break;
	}

}

